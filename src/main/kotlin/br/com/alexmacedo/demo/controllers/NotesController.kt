package br.com.alexmacedo.demo.controllers

import br.com.alexmacedo.demo.models.Note
import br.com.alexmacedo.demo.services.NoteService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/notes")
class NotesController (private val noteService: NoteService) {

    @GetMapping
    fun list(): ResponseEntity<List<Note>> {
        val notes =  noteService.all()
        return ResponseEntity.ok(notes)
    }

    @GetMapping("{id}")
    fun view(@PathVariable id: Long): ResponseEntity<Note> {
        val note = noteService.findById(id)
        note?.let {
            return ResponseEntity.ok(it)
        }
        return ResponseEntity.notFound().build()
    }

    @PutMapping("{id}")
    fun edit(@PathVariable id: Long, @RequestBody note: Note): ResponseEntity<Note> {
        if (noteService.existsById(id)) {
            val updatedNote = noteService.update(id, note)
            return ResponseEntity.ok(updatedNote)
        }
        return ResponseEntity.notFound().build()
    }

    @DeleteMapping("{id}")
    fun remove(@PathVariable id: Long): ResponseEntity<Unit> {
        if (noteService.existsById(id)) {
            noteService.deleteById(id)
            return ResponseEntity.noContent().build()
        }
        return ResponseEntity.notFound().build()
    }

    @PostMapping
    fun add(@RequestBody note: Note): ResponseEntity<Note> {
        val newNote =  noteService.save(note)
        return ResponseEntity.status(HttpStatus.CREATED).body(newNote)
    }

}