package br.com.alexmacedo.demo.controllers

import br.com.alexmacedo.demo.models.User
import br.com.alexmacedo.demo.services.UserService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/users")
class UsersController(private val userService: UserService) {

    @GetMapping
    fun list(): ResponseEntity<List<User>> {
        return ResponseEntity.ok(userService.all())
    }

    @PostMapping
    fun add(@RequestBody user: User): ResponseEntity<User> {
        if (userService.exists(user.username)) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
        }
        val savedUser = userService.save(user)
        return ResponseEntity.ok(savedUser)
    }

}
