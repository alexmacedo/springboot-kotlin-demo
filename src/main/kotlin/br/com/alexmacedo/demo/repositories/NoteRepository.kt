package br.com.alexmacedo.demo.repositories

import br.com.alexmacedo.demo.models.Note
import org.springframework.data.repository.CrudRepository

interface NoteRepository : CrudRepository<Note, Long>