package br.com.alexmacedo.demo.repositories

import br.com.alexmacedo.demo.models.Tag
import org.springframework.data.repository.CrudRepository

interface TagRepository: CrudRepository<Tag, Long>