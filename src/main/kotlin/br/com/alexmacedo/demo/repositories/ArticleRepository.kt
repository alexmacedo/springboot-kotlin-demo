package br.com.alexmacedo.demo.repositories

import br.com.alexmacedo.demo.models.Article
import org.springframework.data.repository.CrudRepository

interface ArticleRepository: CrudRepository<Article, Long>