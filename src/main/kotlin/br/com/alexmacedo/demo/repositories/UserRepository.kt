package br.com.alexmacedo.demo.repositories

import br.com.alexmacedo.demo.models.User
import org.springframework.data.repository.CrudRepository

interface UserRepository: CrudRepository<User, Long> {
    fun findByUsername(username: String): User?
}