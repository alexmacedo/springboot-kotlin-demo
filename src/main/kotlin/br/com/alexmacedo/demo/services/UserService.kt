package br.com.alexmacedo.demo.services

import br.com.alexmacedo.demo.models.User
import br.com.alexmacedo.demo.repositories.UserRepository
import org.springframework.stereotype.Service
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

@Service
class UserService(private val userRepository: UserRepository) {

    fun all(): List<User> = userRepository.findAll().toList()

    fun save(user: User): User {
        val userToSave = User(
                username = user.username,
                password = BCryptPasswordEncoder().encode(user.plainPassword)
        )
        return userRepository.save(userToSave)
    }

    fun exists(username: String): Boolean {
        return userRepository.findByUsername(username) != null
    }

    fun matches(username: String, password: String): Boolean {
        val user = userRepository.findByUsername(username)
        user?.let {
            return BCryptPasswordEncoder().matches(it.password, password)
        }
        return false
    }
}