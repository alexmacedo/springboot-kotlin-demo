package br.com.alexmacedo.demo.services

import br.com.alexmacedo.demo.models.Article
import br.com.alexmacedo.demo.repositories.ArticleRepository
import org.springframework.stereotype.Service

@Service
class ArticleService(private val articleRepository: ArticleRepository) {

    fun all(): List<Article> = articleRepository.findAll().toList()
}