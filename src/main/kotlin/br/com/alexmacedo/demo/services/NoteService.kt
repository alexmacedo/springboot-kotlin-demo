package br.com.alexmacedo.demo.services

import br.com.alexmacedo.demo.models.Note
import br.com.alexmacedo.demo.repositories.NoteRepository
import org.springframework.stereotype.Service

@Service
class NoteService(private val noteRepository: NoteRepository) {

    fun all(): List<Note> = noteRepository.findAll().toList()

    fun deleteById(id: Long) = noteRepository.deleteById(id)

    fun existsById(id: Long): Boolean = noteRepository.existsById(id)

    fun findById(id: Long): Note? = noteRepository.findById(id).orElse(null)

    fun save(note: Note): Note = noteRepository.save(note)

    fun update(id: Long, note: Note): Note {
        val updateNote = Note(id = id, title = note.title, content = note.content)
        return noteRepository.save(updateNote)
    }
}