package br.com.alexmacedo.demo.models

import javax.persistence.*

@Entity
class Article(
        @Id @GeneratedValue val id: Long = 0L,
        val title: String = "",
        val content: String = "",
        @OneToOne
        val author: User,
        @OneToMany
        val tags: List<Tag>
)