package br.com.alexmacedo.demo.models

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Transient

@Entity
class User(
        @Id @GeneratedValue val id: Long = 0L,
        val username: String = "",
        val password: String = "",
        @Transient val plainPassword: String = "")