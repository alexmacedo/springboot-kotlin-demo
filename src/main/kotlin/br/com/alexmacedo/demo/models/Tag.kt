package br.com.alexmacedo.demo.models

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class Tag(@Id @GeneratedValue val id: Long = 0L, val name: String = "")